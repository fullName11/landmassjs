object IntColor {
    val BLACK = from(0f, 0f, 0f, 1f)

    inline fun from(r: Float, g: Float, b: Float, a: Float): Int {
        val rgba = (a.times(255).toInt() shl 24) +
                (r.times(255).toInt() shl 16) +
                (g.times(255).toInt() shl 8) +
                b.times(255).toInt()

        return rgba
    }

    inline fun from(r: Int, g: Int, b: Int, a: Int): Int {
        val rgba = (a shl 24) +
                (r shl 16) +
                (g shl 8) +
                b

        return rgba
    }
}