package utils

import kotlin.math.roundToInt

class PlainArray<T> internal constructor(val cellsWidth: Int, val cellsHeight: Int, init: (Int) -> T) {

    val array: Array<T>

    init {
        array = Array(cellsWidth * cellsHeight, init as (Int) -> Any) as Array<T>
    }

    inline operator fun get(x: Int, y: Int): T? {
        return if (!inBounds(x, y)) {
            null
        } else {
            array[y * cellsWidth + x]
        }
    }

    inline fun getFast(x: Int, y: Int): T {
        return array[y * cellsWidth + x]
    }

    internal fun add(x: Int, y: Int, `val`: T) {
        if (!inBounds(x, y)) {
            return
        }
        array[y * cellsWidth + x] = `val`
    }

    internal operator fun set(x: Int, y: Int, `val`: T) {
        if (!inBounds(x, y)) {
            return
        }
        array[y * cellsWidth + x] = `val`
    }

    internal fun setFast(x: Int, y: Int, `val`: T) {
        array[y * cellsWidth + x] = `val`
    }

    inline fun inBounds(x: Int, y: Int): Boolean {
        return !(x < 0 || x >= cellsWidth || y < 0 || y >= cellsHeight)
    }

    inline fun fori(block: (x: Int, y: Int, v: T) -> Unit) {
        for (y in 0 until cellsHeight) {
            for (x in 0 until cellsWidth) {
                block(x, y, getFast(x, y))
            }
        }
    }

    inline fun foriNoVal(block: (x: Int, y: Int) -> Unit) {
        for (y in 0 until cellsHeight) {
            for (x in 0 until cellsWidth) {
                block(x, y)
            }
        }
    }


    fun set(pos: Point2D, value: T) {
        set(pos.x.roundToInt(), pos.y.roundToInt(), value)
    }

    fun get(pos: Point2D): T? {
        return get(pos.x.roundToInt(), pos.y.roundToInt())
    }

    fun getFastNoRound(pos: Point2D): T {
        return getFast(pos.x.toInt(), pos.y.toInt())
    }

    fun getNoRound(pos: Point2D): T? {
        return get(pos.x.toInt(), pos.y.toInt())
    }

    fun setFastNoRound(pos: Point2D, value: T) {
        setFast(pos.intX, pos.intY, value)
    }

    /**
     * @return null if no min found
     */
    inline fun minNeighbor(
        x: Int,
        y: Int,
        scoreFunction: (x: Int, y: Int, v: T) -> Double,
        filterFunction: (x: Int, y: Int, v: T) -> Boolean,
        resultCallback: (x: Int, y: Int, v: T) -> Unit,
        onFail: () -> Unit
    ) {

        var minScore = Double.MAX_VALUE
        var minX = -1
        var minY = -1
        var minValue: T? = null

        for (dx in x - 1..x + 1) {
            for (dy in y - 1..y + 1) {
                if (dx == x && dy == y) {
                    continue
                }
                get(dx, dy)
                    ?.takeIf { filterFunction(dx, dy, it) }
                    ?.let {
                        val score = scoreFunction(dx, dy, it)
                        if (score < minScore) {
                            minScore = score
                            minX = dx
                            minY = dy
                            minValue = it
                        }
                    }
            }
        }

        if (minX != -1) {
            resultCallback(minX, minY, minValue!!)
            return
        }
        onFail()
    }
}
