import org.w3c.dom.ImageData

class ImageWrapper(val imageData: ImageData) {
    var color: Int = 0

    fun fillRect(x: Int, y: Int, width: Int, height: Int) {

        val r = (color shr 16 and 0xff)
        val g = (color shr 8 and 0xff)
        val b = (color and 0xff)
        val a = (color shr 24 and 0xff)

        val xStart = x.coerceIn(0, CANVAS_WIDTH)
        val xEnd = (x + width).coerceIn(0, CANVAS_WIDTH)
        for (vX in xStart until xEnd) {
            for (vY in y.coerceIn(0, CANVAS_HEIGHT) until (y + height).coerceIn(0, CANVAS_HEIGHT)) {
                fillPixel(vX, vY, r, g, b, a)
            }
        }
    }

    private fun fillPixel(vX: Int, vY: Int, r: Int, g: Int, b: Int, a: Int) {
        val index = ((vY * (imageData.width * 4)) + (vX * 4))
        val imageData = imageData
        js(
            "imageData.data[index] = r\n" +
            "imageData.data[index + 1] = g\n" +
            "imageData.data[index + 2] = b\n" +
            "imageData.data[index + 3] = a\n" +
                "")
    }
}