import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.ImageData
import utils.*
import kotlin.js.Date
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.sin
import kotlin.random.Random

const val MAX_INITIAL_HEIGHT = 2
const val MAP_SIZE = 512
const val CANVAS_WIDTH = (MAP_SIZE * 1.2).toInt()
const val CANVAS_HEIGHT = (MAP_SIZE * 0.5).toInt()

private val BORDER_THREASHOLD = 100
private val SEA_LEVEL = 0.1

class Core {
    lateinit var mainMap: PlainArray<Double>
    lateinit var colorMap: PlainArray<Int>
    lateinit var noiseMaps: List<PlainArray<Double>>
    var maxHeight = 0.0

    private val drawer = Drawer(this)
    private var mapsInited = false

    val dotsPaths = mutableSetOf<Point2D>()

    val random = Random(6)

    fun onTick(delta: Double) {
        doInitIfNeeded()
    }

    private fun doInitIfNeeded() {
        mapsInited.then { return }
        val start = Date.now()
        mapsInited = true

        var size = 4
        noiseMaps = (0..7).map {
            val noiseMap = PlainArray(size, size) { random.nextDouble() * MAX_INITIAL_HEIGHT }
            size *= 2
            noiseMap
        }


        val finalWidth = noiseMaps.maxBy { it.cellsWidth }!!.cellsWidth
        Log.myLog("finalWidth=$finalWidth")
        mainMap = PlainArray(MAP_SIZE, MAP_SIZE) { 0.0 }

        mainMap.fori { x, y, v ->
            var index = 0
            var aggVal = 0.0
            noiseMaps.fori { noiseMap ->
                val octave = (mainMap.cellsWidth / noiseMap.cellsWidth)

                val xi = floor(x / octave.toDouble()).toInt()
                val lerpx = x.toDouble() / octave - xi

                index++
                val yi = floor(y / octave.toDouble()).toInt()
                val lerpy = y.toDouble() / octave - yi

                val xSamples = DoubleArray(4)
                repeat(4) { i ->
                    val default = 0.1

                    xSamples[i] = interpolate(
                        noiseMap.get(xi - 1, yi + i - 1) ?: default,
                        noiseMap.get(xi, yi + i - 1) ?: default,
                        noiseMap.get(xi + 1, yi + i - 1) ?: default,
                        noiseMap.get(xi + 2, yi + i - 1) ?: default,
                        lerpx
                    )
                }
                val noiseVal = interpolate(xSamples[0], xSamples[1], xSamples[2], xSamples[3], lerpy)

                aggVal += noiseVal * (octave)
                //TODO bicubic interpolation
                //TODO isometric draw of landscape
            }
            var finalValue = aggVal
            var distToBorder = minOf(minOf(x, y, mainMap.cellsWidth - x), mainMap.cellsHeight - y)

            if (distToBorder < BORDER_THREASHOLD) {
                val fraction = distToBorder.toFloat() / BORDER_THREASHOLD
                finalValue = aggVal * fraction
            }
            finalValue = maxOf(SEA_LEVEL, finalValue)

            mainMap.setFast(x, y, finalValue)
            maxHeight = maxOf(maxHeight, finalValue)
        }
        simulateErrosion(mainMap)
        Log.myLog("calculated main map, maxHeight=$maxHeight took=${Date.now() - start}")
        createColorMap()
    }

    private fun createColorMap() {
        colorMap = PlainArray(mainMap.cellsWidth, mainMap.cellsHeight, { 0 })

        // TODO smoothstep function

        val seaColor = 0xFF44A7C4.toInt()
        val grassColor = 0xFF699b2c.toInt()
        val grassColor2 = 0xFF2d4a1a.toInt()
        val mountainColor = 0xFF909090.toInt()
        val mountainColor2 = 0xFFb8c0c8.toInt()
        val mountainBrownColor = 0xFF6A5A5D.toInt()
        val snowColor = 0xFFFFFFFF.toInt()
        val rocksColor = 0xFFc7c2bb.toInt()
        val dotPathColor = 0xFF000000.toInt()


        val tmpHeights = mutableListOf<Double>()
        val tmpPos = Point2D(0, 0)
        colorMap.foriNoVal { x, y ->
            tmpPos.set(x, y)

            val height = mainMap.getFast(x, y)
            val normalizedValue = height / maxHeight
            val heightFraction = normalizedValue.toFloat()

            for (xx in x - 1..x + 1) {
                for (yy in y - 1..y + 1) {
                    mainMap.get(xx, yy)?.let { tmpHeights.add(it) }
                }
            }
            val diff = tmpHeights.max()!! - tmpHeights.min()!!
            tmpHeights.clear()
            var isRocky: Double = diff / maxHeight
            isRocky = smoothStep(isRocky.toFloat(), 0.099f, 0.1f).toDouble()

            var finalColor: Int
            finalColor = mixColor(grassColor, grassColor2, smoothStep(heightFraction, 0.0f, 0.50f))
            finalColor = mixColor(finalColor, mountainColor, smoothStep(heightFraction, 0.20f, 0.45f))
            finalColor = mixColor(finalColor, mountainColor2, smoothStep(heightFraction, 0.30f, 0.88f))

            //finalColor = mixColor(
            //    finalColor, mountainBrownColor,
            //    smoothStep(heightFraction, 0.7f, 0.3f) * smoothStep(heightFraction, 0.30f, 0.80f)
            //)

            finalColor = mixColor(finalColor, snowColor, smoothStep(heightFraction, 0.88f, 1f))
            //finalColor = mixColor(finalColor, rocksColor, isRocky.toFloat())

            finalColor = mixColor(
                finalColor,
                seaColor,
                1 - smoothStep(heightFraction, SEA_LEVEL.toFloat(), SEA_LEVEL.toFloat() + 0.0001f)
            )

            if (heightFraction <= SEA_LEVEL.toFloat()) {
                val maxDistFromLand = 20f
                var distanceToLand = getDistanceToLand(mainMap, x, y, SEA_LEVEL) / maxDistFromLand.toDouble()

                if (distanceToLand < 1) {
                    val fadeKoeff = smoothNoiseLayers(x * 2.0, y * 2.0)
                    val fadeKoeffContrast = smoothStep(fadeKoeff.toFloat(), 0.4f, 0.6f)
                    finalColor = mixColor(
                        seaColor, snowColor, (0.5f + sin(distanceToLand * 100) * 0.5f)
                                * (1 - distanceToLand)
                                * fadeKoeffContrast
                    )
                }
            }

            if (dotsPaths.contains(tmpPos)) {
                finalColor = dotPathColor
            }

            colorMap.setFast(x, y, finalColor)
        }
    }

    private fun smoothNoiseLayers(x: Double, y: Double): Double {
        var noooise = smoothNoise(x * 4, y * 4)
        noooise += smoothNoise(x * 8, y * 8) * .5
        noooise += smoothNoise(x * 16, y * 16) * .25
        noooise += smoothNoise(x * 32, y * 32) * .125
        noooise /= 1.875
        return noooise
    }

    private fun getDistanceToLand(map: PlainArray<Double>, x: Int, y: Int, threashold: Double): Int {
        val threashold = threashold + 0.001
        repeat(20) { d ->
            map.get(x, y + d)
                ?.takeIf { it / maxHeight > threashold }
                ?.let { return d }

            map.get(x, y - d)
                ?.takeIf { it / maxHeight > threashold }
                ?.let { return d }

            map.get(x + d, y)
                ?.takeIf { it / maxHeight > threashold }
                ?.let { return d }

            map.get(x - d, y)
                ?.takeIf { it / maxHeight > threashold }
                ?.let { return d }


            map.get(x + d, y + d)
                ?.takeIf { it / maxHeight > threashold }
                ?.let { return d }

            map.get(x + d, y - d)
                ?.takeIf { it / maxHeight > threashold }
                ?.let { return d }

            map.get(x - d, y - d)
                ?.takeIf { it / maxHeight > threashold }
                ?.let { return d }

            map.get(x - d, y + d)
                ?.takeIf { it / maxHeight > threashold }
                ?.let { return d }
        }

        return Int.MAX_VALUE
    }

    private fun smoothNoise(x: Double, y: Double): Double {

        var uvX = x / mainMap.cellsWidth.toDouble()
        var uvY = y / mainMap.cellsHeight.toDouble()

        var lvX = fract(uvX)
        var lvY = fract(uvY)

        lvX = lvX * lvX * (3 - 2.0 * lvX)
        lvY = lvY * lvY * (3 - 2.0 * lvY)


        val idX = floor(uvX)
        val idY = floor(uvY)

        val bl = noise(idX, idY)
        val br = noise(idX + 1, idY)
        val b = mix(bl, br, lvX)

        val tl = noise(idX, idY + 1)
        val tr = noise(idX + 1, idY + 1)
        val t = mix(tl, tr, lvX)

        var c = mix(b, t, lvY)

        return c.toDouble()
        //return lvX.toDouble()
    }

    private fun mix(first: Double, second: Double, fraction: Double): Double {
        return first + (second - first) * fraction
    }

    private fun noise(x: Double, y: Double): Double {
        return fract(sin(x * 101500 + y * 988800.0) * 5656).toDouble()
    }

    private fun fract(d: Double): Double {
        val d = abs(d)
        return (d - d.toInt())
    }

    private fun smoothStep(fraction: Float, start: Float, end: Float): Float {
        val k = maxOf(0.0, minOf(1.0, (fraction - start).toDouble() / (end - start)))

        return ((k * k) * (3 - 2 * k)).toFloat()
    }

    private fun mixColor(c1: Int, c2: Int, percent: Double): Int {

        return mixColor(c1, c2, percent.toFloat())
    }


    private fun mixColor(c1: Int, c2: Int, percent: Float): Int {
        val r1 = (c1 shr 16 and 0xff) / 255f
        val g1 = (c1 shr 8 and 0xff) / 255f
        val b1 = (c1 and 0xff) / 255f

        val r2 = (c2 shr 16 and 0xff) / 255f
        val g2 = (c2 shr 8 and 0xff) / 255f
        val b2 = (c2 and 0xff) / 255f


        val rR: Int = ((r1 + percent * (r2 - r1)) * 255).toInt()
        val rG: Int = ((g1 + percent * (g2 - g1)) * 255).toInt()
        val rB: Int = ((b1 + percent * (b2 - b1)) * 255).toInt()

        return IntColor.from(rR, rG, rB, 255)
    }

    private fun interpolate(a: Double, b: Double, c: Double, d: Double, x: Double): Double {
        val p = d - c - (a - b)
        val result = x * (x * (x * p + (a - b - p)) + (c - a)) + b

        return result
    }

    val imageData = ImageData(CANVAS_WIDTH, CANVAS_HEIGHT)
    var imageWrapper = ImageWrapper(imageData)
    var frame = 0
    fun drawGame(ctx: CanvasRenderingContext2D, delta: Double) {
        frame++

        drawer.draw(imageWrapper, delta)
        ctx.putImageData(imageData, 0.0, 0.0)
    }

    class WaterDot(val pos: Point2D) {
        val prevPos = Point2D(-1, -1)
        var prevHeight: Double = 0.0
        var waterLevel = 1.0
        var groundStorage = 0.0
    }

    private fun simulateErrosion(map: PlainArray<Double>) {
        //repeat(10_0) {
        repeat(1_000_000) {
            val spawnX = random.nextDouble(map.cellsWidth.toDouble()).toInt()
            val spawnY = random.nextDouble(map.cellsHeight.toDouble()).toInt()

            val dot = WaterDot(Point2D(spawnX, spawnY))

            while (true) {
                if (dot.waterLevel <= 0) {
                    break
                }
                val currentHeight = map.get(dot.pos)
                if ((currentHeight.elze { -1.0 } / maxHeight) <= SEA_LEVEL) {
                    break
                }


                map.minNeighbor(dot.pos.roundX, dot.pos.roundY,
                    { x, y, v -> v },
                    { x, y, v -> v <= currentHeight!! && !dot.prevPos.equals(x, y) },
                    { x, y, v ->
                        //   dotsPaths.add(dot.pos.copy())
                        // move dot
                        for (dx in x - 2..x + 2) {
                            for (dy in y - 2..y + 2) {
                                map.get(dx, dy)?.let { oldValue ->
                                    val heightDiffKoeff = currentHeight!! / oldValue
                                    val koeff = (dx.equals(x).and(dy.equals(y)).then { 0.999 } ?: 0.999)
                                    val d = (oldValue * koeff - oldValue) * dot.waterLevel * 0.5 * heightDiffKoeff *
                                            currentHeight!! / maxHeight
                                    var newValue = oldValue + d
                                    val delta = oldValue - newValue

                                    //  println("pick at move x=$dx y=$dy newValue=$newValue oldValue=$oldValue " +
                                    //          "dot.groundStorage=${dot.groundStorage}")

                                    // TODO do not erode more then 5% of original height
                                    dot.groundStorage += delta
                                    map.setFast(dx, dy, newValue)
                                }
                            }
                        }
                        dot.prevHeight = currentHeight!!
                        dot.prevPos.set(dot.pos)
                        dot.pos.set(x.toDouble(), y.toDouble())
                    },
                    {
                        // stop dot
                        val x = dot.pos.roundX
                        val y = dot.pos.roundY
                        for (dx in x - 1..x + 1) {
                            for (dy in y - 1..y + 1) {
                                map.get(dx, dy)?.let { oldValue ->
                                    val koeff = dx.equals(x).and(dy.equals(y)).then { 0.2 } ?: 0.1
                                    val newValue = (oldValue + dot.groundStorage * koeff).coerceAtMost(
                                        maxOf(oldValue, dot.prevHeight)
                                    )
                                    //  println("Drop at stop x=$dx y=$dy newValue=$newValue oldValue=$oldValue")
                                    //  map.setFast(dx, dy, newValue)
                                }
                            }
                        }
                        dot.waterLevel = 0.0
                    })

                dot.waterLevel -= 0.01f
            }

        }
    }
}