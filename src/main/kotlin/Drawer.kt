import utils.PlainArray
import utils.Point2D
import utils.fori
import kotlin.math.sin

private val Y_SCALE_DELTA = 0.01f

class Drawer(val core: Core) {

    private var yScale = 1.0f
    private var yScaleIncrease = true

    var frameTick = 0
    fun draw(ctx: ImageWrapper, delta: Double) {
        frameTick++
        //yScale = (0.5 + sin(frameTick / 10.0)).toFloat()
        yScale = 2.5f
        ctx.color = IntColor.BLACK;
        //  ctx.fillRect(0, 0, ctx.imageData.width, ctx.imageData.height)

        // drawNoiseMaps(ctx)

        //drawFlat(ctx, pixelSize, maxWidth)

        //drawVoxel(maxWidth, ctx, maxWidth, false)
        drawVoxel(ctx, 0, 0, true)
    }

    private fun drawNoiseMaps(ctx: ImageWrapper) {
        var xOffset = 0
        var index = 0
        val pixelSize = 1
        val maxWidth = core.mainMap.cellsWidth * pixelSize

        core.noiseMaps.fori { map ->
            index++
            val scale = maxWidth / map.cellsWidth.toFloat()
            val pixelSize = 1.0 * scale

            map.fori { x, y, v ->

                val colorValue = (v / MAX_INITIAL_HEIGHT).toFloat() / index
                ctx.color = IntColor.from(colorValue, colorValue, colorValue, colorValue)

                ctx.fillRect(
                    (x * scale + xOffset).toInt(), (y * scale).toInt(),
                    pixelSize.toInt(), pixelSize.toInt()
                )
            }
            xOffset += maxWidth
        }
    }

    private val someColor = IntColor.from(0.1f, 0.1f, 0.1f, 1f)

    private fun drawVoxel(
        ctx: ImageWrapper,
        inputXOffset: Int,
        inputYOffset: Int,
        useTopAsFillColor: Boolean
    ) {
        val center = Point2D(core.mainMap.cellsWidth / 2, core.mainMap.cellsHeight / 2)
        val curPos = Point2D(0, 0)
        //TODO try use ImageData
        val maxHeightOfVoxelColumn = 200
        val offsetY = inputYOffset

        val pixelSize = 1.0
        val addSpace = core.mainMap.cellsWidth * 0.3
        forr(addSpace, core.mainMap.cellsWidth, core.mainMap.cellsHeight) { x, y ->

            curPos.set(x.toDouble(), y.toDouble()).minus(center).rotate(frameTick / 90.0).plus(center)

            val finalX = curPos.x.toInt()
            val finalY = curPos.y.toInt()
            val v: Double
            val topColor: Int
            if (finalX < 0 || finalX >= core.mainMap.cellsWidth ||
                finalY < 0 || finalY >= core.mainMap.cellsHeight
            ) {
                v = 0.0
                topColor = 0
                //return@forr
            } else {
                topColor = core.colorMap.getFast(finalX, finalY)
                v = core.mainMap.getFast(finalX, finalY)
            }

            val normalizedValue = v / core.maxHeight
            // println("topColor=${Color.toStr(colorValue, colorValue, colorValue, 1f)}")
            if (useTopAsFillColor) {
                ctx.color = topColor
            } else {
                ctx.color = someColor
            }

            val heightInPoint = (normalizedValue * maxHeightOfVoxelColumn).toInt()

            // draw column
            val fillStartX = (x.plus(addSpace / 2) * pixelSize + inputXOffset).toInt()
            val fillStartY = ((y.plus(addSpace / 3 * 2) * pixelSize) / yScale + offsetY).toInt() - heightInPoint
            val fillWidth = pixelSize.toInt() + 1
            val fillHeight = (heightInPoint * pixelSize).toInt() + 1
           //  ctx.fillRect(fillStartX, fillStartY, fillWidth, fillHeight)

            // draw top voxel
            // TODO draw voxels to fill holes
            ctx.color = topColor
            ctx.fillRect(
                fillStartX,
                ((y.plus(addSpace / 3 * 2) * pixelSize - heightInPoint * pixelSize) / yScale + offsetY - pixelSize).toInt(),
                fillWidth,
                (pixelSize).toInt() + 1
            )
        }
    }

    private inline fun forr(addSpace: Double, width: Int, height: Int, block: (x: Int, y: Int) -> Unit) {
        for (x in -addSpace.toInt() until width + addSpace.toInt()) {
            for (y in -addSpace.toInt() until height + addSpace.toInt()) {
                block(x, y)
            }
        }
    }
}