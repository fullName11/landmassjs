import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document
import kotlin.browser.window
import kotlin.js.Date

class LandmassLooper {
    private var lastTickTs: Double
    private var core: Core
    var canvas = document.getElementById("myCanvas") as HTMLCanvasElement;
    var ctx = canvas.getContext("2d") as CanvasRenderingContext2D

    init {
        core = Core()

        canvas.width = CANVAS_WIDTH
        canvas.height = CANVAS_HEIGHT
        lastTickTs = Date().getTime()
    }

    fun drawLoop() {
        val currentTs = Date().getTime()
        val delta = currentTs - lastTickTs
        core.onTick(delta)
        draw(delta)
        window.requestAnimationFrame { drawLoop() }
    }

    fun draw(delta: Double) {
        core.drawGame(ctx, delta)
    }
}
